const AddUserUseCase = require('../../../../Applications/use_case/AddUserUseCase');

class UsersHandler {
  #container;

  constructor(container) {
    this.#container = container;
  }

  postUsersHandler = async (request, h) => {
    const addUserUseCase = this.#container.getInstance(AddUserUseCase.name);
    const addedUser = await addUserUseCase.execute(request.payload);

    const response = h.response({
      status: 'success',
      data: { addedUser },
    });
    response.code(201);
    return response;
  };
}

module.exports = UsersHandler;

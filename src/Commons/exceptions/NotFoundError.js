const httpStatus = require('http-status');
const ClientError = require('./ClientError');

class NotFoundError extends ClientError {
  constructor(message) {
    super(message, httpStatus.NOT_FOUND);
    this.name = 'NotFoundError';
  }
}

module.exports = NotFoundError;

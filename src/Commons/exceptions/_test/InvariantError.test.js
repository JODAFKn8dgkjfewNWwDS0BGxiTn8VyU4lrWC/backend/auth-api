const httpStatus = require('http-status');
const InvariantError = require('../InvariantError');

describe('InvariantError', () => {
  it('should create an error correctly', () => {
    const invariantError = new InvariantError('an error occurs');

    expect(invariantError.name).toEqual('InvariantError');
    expect(invariantError.message).toEqual('an error occurs');
    expect(invariantError.statusCode).toEqual(httpStatus.BAD_REQUEST);
  });
});

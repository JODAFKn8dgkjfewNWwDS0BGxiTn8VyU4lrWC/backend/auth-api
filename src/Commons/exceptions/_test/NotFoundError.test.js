const httpStatus = require('http-status');
const NotFoundError = require('../NotFoundError');

describe('NotFoundError', () => {
  it('should create NotFoundError correctly', () => {
    const notFoundError = new NotFoundError('not found!');

    expect(notFoundError.name).toEqual('NotFoundError');
    expect(notFoundError.message).toEqual('not found!');
    expect(notFoundError.statusCode).toEqual(httpStatus.NOT_FOUND);
  });
});

const httpStatus = require('http-status');

class ClientError extends Error {
  constructor(message, statusCode = httpStatus.BAD_REQUEST) {
    super(message);

    if (this.constructor.name === 'ClientError') {
      throw new Error('cannot instantiate abstract class');
    }

    this.name = 'ClientError';
    this.statusCode = statusCode;
  }
}

module.exports = ClientError;

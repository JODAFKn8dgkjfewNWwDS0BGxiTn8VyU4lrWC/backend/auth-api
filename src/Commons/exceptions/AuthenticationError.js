const httpStatus = require('http-status');
const ClientError = require('./ClientError');

class AuthenticationError extends ClientError {
  constructor(message) {
    super(message, httpStatus.UNAUTHORIZED);
    this.name = 'AuthenticationError';
  }
}

module.exports = AuthenticationError;

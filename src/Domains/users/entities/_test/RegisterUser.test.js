const RegisterUser = require('../RegisterUser');

describe('RegisterUser', () => {
  it('should throw error when payload did not contain needed property', () => {
    const payload = {
      username: 'johndoe',
      password: 'johndoe',
    };

    expect(() => new RegisterUser(payload)).toThrow('REGISTER_USER.NOT_CONTAIN_NEEDED_PROPERTY');
  });

  it('should throw error when payload did not meet data type specification', () => {
    const payload = {
      username: 'johndoe',
      password: 'johndoe',
      fullname: 123,
    };

    expect(() => new RegisterUser(payload)).toThrow('REGISTER_USER.NOT_MEET_DATA_TYPE_SPECIFICATION');
  });

  it('should throw error when username contains more than 50 characters', () => {
    const payload = {
      username: 'qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbn',
      password: 'johndoe',
      fullname: 'Johnny Doe',
    };

    expect(() => new RegisterUser(payload)).toThrow('REGISTER_USER.USERNAME_LIMIT_CHAR');
  });

  it('should throw error when username contains restricted character', () => {
    // Arrange
    const payload = {
      username: 'john doe',
      fullname: 'johndoe',
      password: 'Johnny Doe',
    };
    // Action and Assert
    expect(() => new RegisterUser(payload)).toThrow('REGISTER_USER.USERNAME_CONTAIN_RESTRICTED_CHARACTER');
  });

  it('should create registerUser object correctly', () => {
    const payload = {
      username: 'johndoe',
      password: 'johndoe',
      fullname: 'Johnny Doe',
    };

    const { username, password, fullname } = new RegisterUser(payload);

    expect(username).toEqual(payload.username);
    expect(password).toEqual(payload.password);
    expect(fullname).toEqual(payload.fullname);
  });
});

const PasswordHash = require('../../Applications/security/PasswordHash');

class PasswordHashBcrypt extends PasswordHash {
  #bcrypt;
  #saltRound;

  constructor(bcrypt, saltRound = 10) {
    super();
    this.#bcrypt = bcrypt;
    this.#saltRound = saltRound;
  }

  async hash(password) {
    return this.#bcrypt.hash(password, this.#saltRound);
  }
}

module.exports = PasswordHashBcrypt;

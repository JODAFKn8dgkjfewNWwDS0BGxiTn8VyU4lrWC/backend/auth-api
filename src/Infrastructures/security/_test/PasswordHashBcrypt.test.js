const bcrypt = require('bcrypt');
const PasswordHashBcrypt = require('../PasswordHashBcrypt');

describe('PasswordHash', () => {
  describe('hash function', () => {
    it('should encrypt password correctly', async () => {
      // Arrange
      const spyHash = jest.spyOn(bcrypt, 'hash');
      const passwordHashBcrypt = new PasswordHashBcrypt(bcrypt);

      // Action
      const encryptedPassword = await passwordHashBcrypt.hash('plain_password');

      // Assert
      expect(typeof encryptedPassword).toEqual('string');
      expect(encryptedPassword).not.toEqual('plain_password');
      expect(spyHash).toHaveBeenCalledWith('plain_password', 10);
    });
  });
});

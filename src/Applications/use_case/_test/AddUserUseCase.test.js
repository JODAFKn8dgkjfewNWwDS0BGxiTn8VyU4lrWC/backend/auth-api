const UserRepository = require('../../../Domains/users/UserRepository');
const RegisteredUser = require('../../../Domains/users/entities/RegisteredUser');
const PasswordHash = require('../../security/PasswordHash');
const RegisterUser = require('../../../Domains/users/entities/RegisterUser');
const AddUserUseCase = require('../AddUserUseCase');

describe('AddUserUseCase', () => {
  it('should orchestrating the add user action correctly', async () => {
    // Arrange
    const useCasePayload = {
      username: 'johndoe',
      password: 'johndoe',
      fullname: 'Johnny Doe',
    };
    const mockRegisteredUser = new RegisteredUser({
      id: 'user-123',
      username: useCasePayload.username,
      fullname: useCasePayload.fullname,
    });

    /** creating dependency of use case */
    const mockUserRepository = new UserRepository();
    const mockPasswordHash = new PasswordHash();

    /** mocking needed functions */
    mockUserRepository.addUser = jest.fn().mockImplementation(() => Promise.resolve(mockRegisteredUser));
    mockUserRepository.verifyAvailableUsername = jest.fn().mockImplementation(() => Promise.resolve());
    mockPasswordHash.hash = jest.fn().mockImplementation(() => Promise.resolve('encrypted_password'));

    /** creating use case instance */
    const addUserUseCase = new AddUserUseCase({
      userRepository: mockUserRepository,
      passwordHash: mockPasswordHash,
    });

    // Action
    const registeredUser = await addUserUseCase.execute(useCasePayload);

    // Assert
    expect(registeredUser).toStrictEqual(mockRegisteredUser);
    expect(mockPasswordHash.hash).toHaveBeenCalledWith(useCasePayload.password);
    const addUserPayload = {
      username: useCasePayload.username,
      password: 'encrypted_password',
      fullname: useCasePayload.fullname,
    };
    expect(mockUserRepository.addUser).toHaveBeenCalledWith(new RegisterUser(addUserPayload));
  });
});
